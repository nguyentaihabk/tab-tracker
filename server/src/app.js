const express = require('express')
const morgan = require('morgan')
const bodyParser = require('body-parser')
const cors = require('cors')

const app = express()

app.use(morgan('combined'))
app.use(cors())
app.use(bodyParser.json())

app.get('/', (req, res) => {
    return res.send('test ok')
})

app.listen(process.env.PORT || 8081, () => {
    console.log('connected');
})

